import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'

import injectGlobal         from './injectGlobal'

import Header               from 'app/page/web/header/Header'
import Aside                from 'app/page/web/Aside/Aside'

import go                   from 'engine/history/go'


const Wrapper = styled.div`
  background-color: #f1f4f8;
  
  width: 100%;
  height: 100%;
`

const WrapperLogin = styled.div`
  width: 100%;
  height: 100%;
`

const Content = styled.div`
  width: ${ props => props.shift ? 'calc(100% - 320px)' : '100%' };
  margin-left: ${ props => props.shift ? '320px' : '0px' };
  height: 100%;
`

class App extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
    }
  }


  onHandleOpenAside = ({ open }) => this.setState({ open })

  render() {
    const { children, title } = this.props

    const { open } = this.state

    if (title === 'Login') return (
      <WrapperLogin>
        { children }
      </WrapperLogin>
    )

    return (
      <Wrapper>
        <Header openAside={ this.onHandleOpenAside } />
        <Content shift={ open }>
        { children }
        </Content>
        <Aside open={ open } />
      </Wrapper>
    )
  }
}

const mapStateToProps = state => {
  const { login: { userLogin } } = state

  const { order } = userLogin

  return {
    isAuth: order.length > 0
  }
}

export default compose(
  connect(mapStateToProps)
)(App)
