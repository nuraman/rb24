import { injectGlobal }     from 'styled-components'


import RalewayRegular       from 'font/Raleway-Regular.ttf'
import RalewayLight         from 'font/Raleway-Light.ttf'
import RalewayItalic        from 'font/Raleway-Italic.ttf'
import RalewayBold          from 'font/Raleway-Bold.ttf'
import RalewayExtraBold     from 'font/Raleway-ExtraBold.ttf'
import RalewaySemiBold      from 'font/Raleway-SemiBold.ttf'

import ProximaNovaRegular   from 'font/ProximaNova/ProximaNova-Regular.ttf'
import ProximaNovaLight     from 'font/ProximaNova/ProximaNova-Light.ttf'
import ProximaNovaBold      from 'font/ProximaNova/ProximaNova-Bold.ttf'
import ProximaNovaSemiBold  from 'font/ProximaNova/ProximaNova-Semibold.ttf'
import ProximaNovaExtraBold from 'font/ProximaNova/ProximaNova-Extrabld.ttf'


export default injectGlobal`
  body {
    width: 100%;
    height: 100vh;
    margin: 0;
    overflow: hidden;
    @font-face {
      font-family: 'RalewayRegular';
      src: url(${ RalewayRegular }) format('truetype');
    }
    @font-face {
      font-family: 'RalewayLight';
      src: url(${ RalewayLight }) format('truetype');
    }
    @font-face {
      font-family: 'RalewayBold';
      src: url(${ RalewayBold }) format('truetype');
    }
    @font-face {
      font-family: 'RalewayExtraBold';
      src: url(${ RalewayExtraBold }) format('truetype');
    }
    @font-face {
      font-family: 'RalewaySemiBold';
      src: url(${ RalewaySemiBold }) format('truetype');
    }
    @font-face {
      font-family: 'ProximaNovaRegular';
      src: url(${ ProximaNovaRegular }) format('truetype');
    }
    @{
      font-family: 'ProximaNovaSemiBold';
      src: url(${ ProximaNovaSemiBold }) format('truetype');
    }
    @{
      font-family: 'ProximaNovaBold';
      src: url(${ ProximaNovaBold }) format('truetype');
    }
    @{
      font-family: 'ProximaNovaExtraBold';
      src: url(${ ProximaNovaExtraBold }) format('truetype');
    },
  },
   input,
  select,
  textarea {
    border: none;
    outline: none;
    -webkit-appearance: none;
  }
  
  input::-webkit-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }

  input::-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-moz-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:-ms-input-placeholder {
    opacity: 1;
    font-weight: 300;
    font-size: 18px;
    color: #fff;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-webkit-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus::-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-moz-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
  
  input:focus:-ms-input-placeholder {
    opacity: 0;
    transition: opacity 0.2s ease;
  }
`
