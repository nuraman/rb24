import React                from 'react'
import ReactDOM             from 'react-dom'

import { Provider }         from 'react-redux'
import { createStore }      from 'redux'

import queryString          from 'query-string'
import Router               from 'universal-router'


import
    registerServiceWorker   from './registerServiceWorker'

import App                  from './App'

import createReducer        from './createReducer'

import history              from 'engine/history'
import routes               from 'app/page/web/routers'


registerServiceWorker()

const store = createStore(createReducer)

const container = document.getElementById('root')

const router = new Router(routes)

let currentLocation = history.location

async function onLocationChange(location, action) {
  currentLocation = location

  try {
    const route = await router.resolve({
      pathname: location.pathname,
      query: queryString.parse(location.search)
    })

    if (currentLocation.key !== location.key) return

    if (route.redirect) {
      history.replace(route.redirect)

      return
    }

    ReactDOM.render(
      <Provider store={ store }>
        <App title={ route.title }>
          { route.component }
        </App>
      </Provider>,
      container
    )
  } catch (error) {
    console.error(error)
  }
}

history.listen(onLocationChange)
onLocationChange(currentLocation)

