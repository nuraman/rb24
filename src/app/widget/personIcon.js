import styled             from "styled-components"


const PersonIcon = styled.div`
  min-width: 16px;
  width: 16px;
  height: 16px;
  
  margin: 0px 10px 0px 10px;
  
  background-image: url(assets/upTrend.svg);
  background-size: cover;
`

export default PersonIcon