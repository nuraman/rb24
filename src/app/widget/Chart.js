import Highcharts           from 'highcharts'

import PersonIcon           from './personIcon'


const trendMap = {
  UP: 'up',
  DOWN: 'down',
  NOTHING: '-'
}


function Chart() {
  this.series = []
  this.execution = ''
  this.id = ''
  this.name = ''
  this.height = ''
  this.completed = false
  this.trend = ''
  this.fact = ''
  this.plan = ''
  this.measure = ''
  this.kpiSalay = true

}

Chart.prototype = {
  set options(payload) {
    const { id, execution, completed, trend, fact, plan, height, title, measure, kpiSalay } = payload

    this.id = id
    this.fact = fact
    this.name = title
    this.height = height
    this.completed = completed
    this.trend = trend
    this.execution = execution
    this.plan = plan
    this.measure = measure
    this.kpiSalay = kpiSalay
  }
}


Chart.prototype.getSeries = function () {
  let metric = ''
  return [{
    name: this.name,
    data: [{
      id: this.id,
      y: this.fact,
      value: this.plan,
      color: this.completed ?  '#4fc4e4' : '#ec6683',
      radius: '105%',
      innerRadius: '88%',
      plan: this.plan
    }],
    dataLabels: {
      useHTML: true,
      formatter: () => {
        if (this.measure === 'руб.') {
          metric = `<div 
                        style="min-width: 16px;
                        width: 16px; 
                        vertical-align: middle; 
                        height: 16px;
                        margin: 0px 10px 0px 10px;
                        background-image: url(assets/ruble.svg);
                        background-size: cover;">
                    </div>`
        } else {
          metric = this.measure
        }
        if (this.trend === trendMap.UP) {
        return `<div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center">
                        <div style="min-width: 16px;width: 16px; vertical-align: middle; height: 12px;margin: 0px 10px 0px 10px;background-image: url(assets/upTrend.svg);background-size: cover;"></div> 
                        <div style="display:flex; flex-direction: row; align-items: center;">
                            ${this.execution} ${metric}
                        </div>
                    </div>`
        }

        if (this.trend === trendMap.DOWN) {
          return `<div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center">
                        <div style="display:flex; flex-direction: row; align-items: center;">
                            ${this.execution} ${metric}
                        </div>                        
                        <div style="min-width: 16px;width: 16px; vertical-align: middle; height: 12px;margin: 0px 10px 0px 10px;background-image: url(assets/downTrend.svg);background-size: cover;"></div> 
                    </div>`
        }

        if (!this.trend || this.trend === trendMap.NOTHING) {
          return `<div style="display: flex; flex-direction: column; justify-content: space-around; align-items: center">
                        <div style="display: flex; flex-direction: row; align-items: center;">
                            ${this.execution} ${metric}
                        </div>
                  </div>`
        }
      },
      style: {
        textShadow: false,
        color: 'black',
        fontSize: '36px',
        fontFamily: 'ProximaNovaBold',
      },
      borderWidth: 0
    },
  }]
}

Chart.prototype.getConfig = function () {
  const series = this.getSeries()
  const plan = this.plan
  const config = {
    chart: {
      height: `${ this.height }px`,
      type: 'solidgauge',
      backgroundColor: this.kpiSalay ? '#fff' : '#54adce'
    },
    title: null,
    tooltip: {
      enabled: false,
    },
    pane: {
      startAngle: -140,
      endAngle: 140,
      background: [{
        outerRadius: '112%',
        innerRadius: '90%',
        backgroundColor: '#f7f7f7',
        borderWidth: 0
      }]
    },
    yAxis: [{
      lineWidth: 0,
      tickPosition: 'outside',
      tickInterval: this.plan,
      tickWidth: 0,
      startOnTick: false,
      min: 0,
      max: this.plan,
      labels: {
        style: {
          fontFamily: 'ProximaNovaRegular',
          fontSize: '14px',
        },
        formatter: function() {
          if (this.value == plan) {
            return this.value + ' Р'
          }

          return this.value
        }
      }
    }],
    credits: {
      enabled: false
    },
    plotOptions: {
      solidgauge: {
        dataLabels: {
          enabled: true,
          useHTML: true,
          padding: 0,
          align: 'center',
          verticalAlign: 'middle',
          y: -5,
        },
        label: {
          enabled: false,
        },
        enableMouseTracking: false,
        linecap: 'round',
        stickyTracking: false,
        rounded: true
      }
    },
    series: series
  }

  return config
}

export default Chart
