import React                from 'react'

import styled               from 'styled-components'


import ProximaNovaBold      from 'font/ProximaNova/ProximaNova-Bold.ttf'


const Container = styled.div`
  display: block;
  padding: 10px 10px 10px 36px;
  @{
    font-family: 'ProximaNovaBold';
    src: url(${ ProximaNovaBold }) format('truetype');
  }
`

const RowFirst = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 30px;
  width: 100%;
`

const Persent = styled.label`
  font-family: ProximaNovaRegular;
  font-size: 16px;
  font-weight: bold;
  color: #000;
`

const Title = styled.label`
  font-family: ProximaNovaRegular;
  font-size: 16px;
  color: #a7a7a7;
  
  padding-left: 10px;
`

const RowSecond = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 100%;
  height: 30px;
`

const UpIcon = styled.div`
  min-width: 16px;
  width: 16px; 
  vertical-align: middle; 
  height: 12px;
  margin: 0px 10px 0px 10px;
  background-image: url(assets/upTrend.svg);
  background-size: cover;
`

const DownIcon = styled.div`
  min-width: 16px;
  width: 16px; 
  vertical-align: middle; 
  height: 12px;
  margin: 0px 10px 0px 10px;
  background-image: url(assets/downTrend.svg);
  background-size: cover;
`

const RowIndicator = styled.div`
  display: flex;
  flex-direction: row;

  border-style: solid;
  border-width: 1px;
  border-color: rgb(228, 246, 251);
  border-radius: 10px;
  background-color: rgba(255, 255, 255, 0);
  width: 243px;
  height: 18px;
`

const RowIndicatorChart = styled.div`
  display: flex;
  flex-direcion: row;
  align-items: center;

  background-image: -moz-linear-gradient( 0deg, rgb(218,28,167) 0%, rgb(250,125,125) 100%);
  background-image: -webkit-linear-gradient( 0deg, rgb(218,28,167) 0%, rgb(250,125,125) 100%);
  background-image: -ms-linear-gradient( 0deg, rgb(218,28,167) 0%, rgb(250,125,125) 100%);
  width: ${ props=>props.width }px;
  height: 16px;
  border-radius: 10px;
`

const RowIndicatorChartLabel = styled.label`
  font-family: ProximaNovaRegular;
  font-size: 14px;
  color: #fff;
  margin-left: auto;
  z-index: 100;
  padding: 1px;
`

const RowIndicatorLabel = styled.label`
  font-family: ProximaNovaRegular;
  font-size: 14px;
  color: #000;
  margin-left: auto;
  z-index: 300;
`

const WrapperIcon = styled.div`
  border-style: solid;
  border-width: 1px;
  border-color: rgb(213, 218, 220);
  border-radius: 3px;
  background-color: rgba(255, 255, 255, 0);
  width: 34px;
  height: 34px;
  z-index: 133;
  
  margin-left: auto;
  
  display: flex;
  align-items: center;
  justify-content: space-around;
`

const ChartIcon = styled.div`
  min-width: 23px;
  width: 23px;
  height: 15px;
  
  cursor: pointer;
  
  background-image: url(assets/chart.svg);
  background-size: cover;
`

export default (props) => {
  const {
    id,
    kpiType,
    title,
    trend,
    plan,
    fact,
    execution,
    completed,
    kpiSalay,
    measure,
    parent,
    onOpenChart,
    onSelect,
    last
  } = props

  const onChartClick = () => onOpenChart()

  const onHandleClick = () => {
    if (!last) onSelect({ id })
  }

  return (
    <Container onClick={ onHandleClick }>
      <RowFirst>
        <Persent>
          { `${ execution } ${ measure }` }
        </Persent>
        <Title>
          { title }
        </Title>
      </RowFirst>
      <RowSecond>
        {
          trend ? <UpIcon /> : <DownIcon />
        }
        <RowIndicator>
          <RowIndicatorChart width={ fact ? fact : '20' }>
            <RowIndicatorChartLabel>
              { fact }
            </RowIndicatorChartLabel>
          </RowIndicatorChart>
          <RowIndicatorLabel>
            { plan }
          </RowIndicatorLabel>
        </RowIndicator>
        <WrapperIcon>
          <ChartIcon onClick={ onChartClick } />
        </WrapperIcon>
      </RowSecond>
    </Container>
  )
}