import React, { Component } from 'react'

import styled               from 'styled-components'


import WidgetHeader         from 'app/widget/web/GoodExchange/WidgetHeader'
import BlockKpi             from './BlockKpi'


const Wrapper = styled.div`
  background-color: rgb(251, 251, 251);
  box-shadow: 0px 10px 65px 0px rgba(20, 124, 157, 0.12);
  width: 410px;
  height: 350px;
  z-index: 32;
  
  display: flex;
  justify-content: space-around;
  align-items: center;
  
  margin-bottom: 10px;
`

const Widget = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const WidgetContent = styled.div`
  display: flex;
  width: 100%;
  height: calc(100% - 50px);
  align-items: center;
  justify-content: space-around;
  flex-direction: column;
  cursor: pointer;
`

class Container extends Component {
  constructor() {
    super()

    this.state = {
      dataList: []
    }
  }

  componentWillMount() {
    const { dataList } = this.props

    this.setState({ dataList })
  }

  componentWillReceiveProps(nextProps) {
    const { dataList } = nextProps

    this.setState({ dataList })
  }

  onHandleSelect = ({ id }) => {
    const { onSelect } = this.props || {}

    if (onSelect) {
      onSelect({ id })
    }
  }

  onHandleOpenChart = () => {

  }

  render() {
    const { dataList } = this.state

    return (
      <Wrapper>
        <Widget>
          <WidgetHeader title='По филиалам' color='true' />
          <WidgetContent>
            {
              dataList.map((item, index) => {
                return (
                  <BlockKpi
                    key={ index }
                    { ...item }
                    onOpenChart={ this.onHandleOpenChart }
                    onSelect={ this.onHandleSelect }
                  />)
              })
            }
          </WidgetContent>
        </Widget>
      </Wrapper>
    )
  }
}

export default Container