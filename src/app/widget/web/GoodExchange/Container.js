import React, { Component } from 'react'

import ReactHighcharts      from 'react-highcharts'
import HighchartsMore       from 'highcharts/highcharts-more'
import SolidGauge           from 'highcharts/modules/solid-gauge'

import styled               from 'styled-components'

import Chart                from 'app/widget/Chart'

import WidgetHeader         from './WidgetHeader'
import WidgetFooter         from './WidgetFooter'


HighchartsMore(ReactHighcharts.Highcharts)
SolidGauge(ReactHighcharts.Highcharts)


const Wrapper = styled.div`
  background-color: ${ props=>props.color == 'true' ? 'rgb(251, 251, 251)' : '#54adce' };
  box-shadow: 0px 10px 65px 0px rgba(20, 124, 157, 0.12);
  width: 410px;
  height: 350px;
  z-index: 32;
  
  margin-bottom: 10px;
`

const Widget = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const WidgetContent = styled.div`
  display: block;
  width: 100%;
  
  background-color: ${ props=>props.white ? '#fff' : '#54adce' }
  
  cursor: pointer;
`


const type = {
  PLANNED: 'planned',
  NORMALIZED: 'normalized'
}

class Container extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      chart: {},
      config: {},
      kpiSalay: false,
      id: '',
      chartHeight: 250,
      measure: ''
    }
  }

  componentWillMount() {
    const {
      id,
      kpiType,
      title,
      trend,
      plan,
      fact,
      execution,
      completed,
      kpiSalay,
      measure,
      parent
    } = this.props

    const { chartHeight } = this.state

    const chart = new Chart()

    if (kpiType === type.PLANNED) {
      chart.options = { id, execution, completed, trend, fact, plan, height: chartHeight, measure, kpiSalay }
    }

    if (kpiType === type.NORMALIZED) {
      chart.options = {id, execution, completed, trend, fact, plan, height: chartHeight, measure, kpiSalay }
    }

    const config = chart.getConfig()

    this.setState({
      title,
      chart,
      config,
      kpiSalay,
      id
    })
  }

  componentWillReceiveProps(nextProps) {
    const {
      id,
      kpiType,
      title,
      trend,
      plan,
      fact,
      execution,
      completed,
      kpiSalay,
      measure,
      parent
    } = nextProps

    const { chartHeight } = this.state

    const chart = new Chart()

    if (kpiType === type.PLANNED) {
      chart.options = { id, execution, completed, trend, fact, plan, height: chartHeight, measure, kpiSalay }
    }

    if (kpiType === type.NORMALIZED) {
      chart.options = {id, execution, completed, trend, fact, plan, height: chartHeight, measure, kpiSalay }
    }

    const config = chart.getConfig()

    this.setState({
      title,
      chart,
      config,
      kpiSalay,
      id
    })
  }

  onHandleClick = () => {
    const {id } = this.state

    const { onSelect } = this.props

    onSelect({ id })
  }

  openChart = () => {

  }

  render() {
    const { title, config, kpiSalay } = this.state

    return (
      <Wrapper color={ kpiSalay.toString() }>
        <Widget>
          <WidgetHeader title={ title } color={ kpiSalay.toString() } />
          <WidgetContent
            white={ kpiSalay }
            onClick={ this.onHandleClick }
          >
            <ReactHighcharts config={ config } />
          </WidgetContent>
          <WidgetFooter onClick={ this.openChart } />
        </Widget>
      </Wrapper>
    )
  }
}

export default Container