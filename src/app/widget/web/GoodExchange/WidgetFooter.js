import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: block;
  height: 50px;
`

const Right = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin-left: auto;
  
  width: 50px;
  height: 50px;
`

const WrapperIcon = styled.div`
  border-style: solid;
  border-width: 1px;
  border-color: rgb(213, 218, 220);
  border-radius: 3px;
  background-color: rgba(255, 255, 255, 0);
  width: 34px;
  height: 34px;
  z-index: 133;
  
  display: flex;
  align-items: center;
  justify-content: space-around;
`

const ChartIcon = styled.div`
  min-width: 23px;
  width: 23px;
  height: 15px;
  
  cursor: pointer;
  
  background-image: url(assets/chart.svg);
  background-size: cover;
`

export default (props) => {
  const { onClick } = props

  const onHandleClick = () => onClick()

  return (
    <Container>
      <Right>
        <WrapperIcon>
          <ChartIcon onClick={ onHandleClick }/>
        </WrapperIcon>
      </Right>
    </Container>
  )
}