import React                from 'react'

import styled               from 'styled-components'


const WidgetHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  height: 50px;
  width: 100%;
  
  position: relative;
`

const Title = styled.label`
  font-family: RalewaySemiBold;
  font-size: 16px;
  color: ${ props=>props.color == 'true' ? '#000' : '#fff' };
`

const PointIcon = styled.div`
  position: absolute;
  right: 0;
  
  min-width: 16px;
  width: 35px;
  height: 28px;
  
  cursor: pointer;
  
  margin: 0px 10px 0px 10px;
  
  background-image: url(assets/points.svg);
  background-size: cover;
`

export default (props) => {
  const { title, color } = props

  return (
    <WidgetHeader>
      <Title color={ color }>
        { title }
      </Title>
      <PointIcon />
    </WidgetHeader>
  )
}