import React                from 'react'

import styled               from 'styled-components'


const Wrapper = styled.div`
  border: 1px solid transparent;
  width:  190px;
  height: 40px;
`

const Button = styled.input.attrs({
  type: 'button'
})`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  text-align: center;
  font-weight: 300;
  font-family: HelveticaRegular;
  font-size: 18px;
  border: none;
  outline: none;
  color: #fff;
  background-color: #bf4a49;
  cursor: pointer;
    
  &:hover {
    background-color: #c95453;
  }
  
  &:focus, &:active {
    border: none;
    outline: none;
  }
`

export default (props) => {
  const { onSubmit } = props

  const onHandleClick = () => onSubmit()

  return (
    <Wrapper>
      <Button onClick={ onHandleClick } value='Войти' />
     </Wrapper>
  )
}