import React, { Component } from 'react'


import Login                from './Login'


const title = 'Login'

export default {
  path: '/Login',
  action: () => {
    return {
      title,
      component: (<Login />)
    }
  }
}