import React, { Component } from 'react'

import styled               from 'styled-components'


const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  border: 1px solid #f9f9f9;
  width: 47%;
  height: 38px;
`

const PersonIcon = styled.div`
  min-width: 16px;
  width: 40px;
  height: 100%;
  
  background: center no-repeat;
  
  background-image: url(assets/password.png);
  background-size: 18px 11px;
  
`

const Input = styled.input.attrs({
  type: 'password'
})`
    width: 100%;
    height: 100%;
    text-decoration: none;
    background-color: transparent;
    border: 0px;
    outline: none;
    font-family: RalewayRegular;
    font-size: 16px;
    color: #fff;
`

class InputLogin extends Component {
  constructor() {
    super()

    this.state = {
      value: ''
    }
  }

  componentWillMount() {
    const { value } = this.props

    this.setState({ value })
  }

  onChange = ({ target: { value } }) => {
    const { onChange } = this.props

    this.setState({ value })

    onChange({ value })
  }

  render() {
    const { value } = this.state

    return (
      <Wrapper>
        <PersonIcon/>
        <Input
          value={ value }
          onChange={ this.onChange }
          className='password'
        />
      </Wrapper>
    )
  }
}

export default InputLogin