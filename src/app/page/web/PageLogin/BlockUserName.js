import React, { Component } from 'react'

import styled               from 'styled-components'


const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  border: 1px solid #f9f9f9;
  width: 47%;
  height: 38px;
`

const PersonIcon = styled.div`
  min-width: 16px;
  width: 16px;
  height: 16px;
  
  margin: 0px 10px 0px 10px;
  
  background-image: url(assets/person.svg);
  background-size: cover;
`

const Input = styled.input.attrs({
  type: 'text'
})`
    width: 100%;
    height: 100%;
    text-decoration: none;
    background-color: transparent;
    border: 0px;
    outline: none;
    font-family: RalewayRegular;
    font-size: 16px;
    color: #fff;
`

class InputLogin extends Component {
  constructor() {
    super()

    this.state = {
      value: ''
    }
  }

  componentWillMount() {
    const { value } = this.props

    this.setState({ value })
  }

  onChange = ({ target: { value } }) => {
    const { onChange } = this.props

    this.setState({ value })

    onChange({ value })
  }

  render() {
    const { value } = this.state

    return (
      <Wrapper>
        <PersonIcon/>
        <Input
          value={ value }
          onChange={ this.onChange }
        />
      </Wrapper>
    )
  }
}

export default InputLogin