const proxyUrl = 'https://cors-anywhere.herokuapp.com/'

/*
{
    "data": {
        "full_name": "Иванов Алекандр",
        "position": "Генеральный директо"
    }
}

{
    "errors": {
        "base": [
            "Неправильный пароль"
        ]
    }
}
 */

export default async (payload) => {
  const { path, data, order } = payload

  const dataResponce = {}

  let url = `${ path }?` + order.reduce((paramList, id, idx) => {
    const { name, value } = data[id]

    if (idx === order.length - 1) {
      paramList += `${ name }=${ value }`
    } else {
      paramList += `${ name }=${ value }&`
    }

    return paramList
  }, ``)

    const responce = await fetch(proxyUrl + url, {
      method: 'POST',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })

    return await responce.json()
      .then((responce) => {
        return responce.json()
      })
      .then((result) => {

        if (result.data) {
          const { auth_token, full_name, position } = result.data

          dataResponce.authToken = auth_token
          dataResponce.userName = full_name
          dataResponce.position = position
        }

        if (result.errors) {
          const { errors: { base } } = result
          dataResponce.error = base
        }
      })
      .catch((e) => {
        console.log(e)
      })

  return dataResponce
}