import _update              from 'immutability-helper'
import uuidv4               from 'uuid/v4'

const name = `login`

const actionMap = {
  LOGIN: `${ name }/Login`,
  LOGOFF: `${ name }/LOGOFF`
}

const STATE = {
  userLogin: {
    data: {},
    order: []
  },
  userLogOut: {
    data: {},
    order: []
  }
}

const reducer = (state=STATE, action) => {
  const { type, payload } = action

  const { userLogin, userLogOut } = state

  switch (type) {

    case actionMap.LOGIN: {
      const { authToken, userName, position } = payload

      const id = uuidv4()

      const newUserLogin = _update(userLogin, {
        data: { $merge: { [id]: { authToken, userName, position } } },
        order: { $push: [id] }
      })

      return {
        ...state,
        userLogin: newUserLogin
      }
    }
    case actionMap.LOGOFF: {
      break
    }

    default:
      return state
  }
}

export default {
  reducer,
  actionMap
}