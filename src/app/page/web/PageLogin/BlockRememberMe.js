import React                from 'react'

import styled               from 'styled-components'


const WrapperCheckbox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 50%;
`

const Checkbox = styled.input.attrs({
  type: 'checkbox',
})`
  display: none;
`

const LabelNewCheck = styled.label`
  position: relative;
  width: 18px;
  min-width: 18px;
  height: 18px;
  margin-right: 10px;
  border: 1px solid #48b9bf;
  cursor: pointer;

  &:before {
    display: ${ props => props.checked ? 'block' : 'none' };
    content: "";
    position: absolute;
    width: 12px;
    height: 12px;
    top: 3px;
    left: 3px;
    background-color: #48b9bf;
    
    ${ Checkbox }:checked + & {
      display: block;
    }  
  } 
`

const Label = styled.label`
  margin-bottom: -2px;
  font-weight: 100;
  font-size: 18px;
  font-family: RalewayRegular;
  color: #fff;
  cursor: pointer;
`

export default (props) => {
  const { remember, onRemember } = props

  const onChange = ({ target }) => {
    onRemember({ remember: target.checked })
  }

  return (
   <WrapperCheckbox>
     <Checkbox id='remember' onChange={ onChange } />
     <LabelNewCheck htmlFor='remember' checked={ remember } />
     <Label htmlFor='remember'>Запомнить меня</Label>
   </WrapperCheckbox>
  )
}
