import React                from 'react'

import styled               from 'styled-components'


const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  width: 100%;
  height: 120px;
`

const Logo = styled.div`
  width: 100px;
  height: 60px;
  
  background: center no-repeat;
  background-image: url(assets/logo.png);
  background-size: 100px 60px;
`

export default () => {
  return (
    <Wrapper>
      <Logo />
    </Wrapper>
  )
}