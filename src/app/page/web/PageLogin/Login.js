import React, { Component } from 'react'

import uuid                 from 'uuid'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'


import BlockSubmit          from './BlockSubmit'
import BlockUserName        from './BlockUserName'
import BlockPassword        from './BlockPassword'
import BlockLogo            from './BlockLogo'
import BlockRememberMe      from './BlockRememberMe'

import reducerLogin         from './reducer'
import reducerPeriod        from 'app/page/web/header/reducer'
import go                   from 'engine/history/go'


const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  
  width: 100%;
  height: 100%;
  
  background-color: #6884a8;
`

const WrapperContent = styled.div`
  display: block;
  height: 320px;
  width: 600px;
  
  background-color: #383838;
`

const Content = styled.div`
  margin: 20px 50px 20px 50px;
`

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 20px;
`

const Error = styled.div`
  font-family: RalewayBold;
  font-size: 16px;
  color: #bf4a49;
`

const url = `https://rb24.herokuapp.com/login`
const proxyUrl = 'https://cors-anywhere.herokuapp.com/'

const api = async (payload) => {
  const { path, data, order } = payload

  const dataResponce = {}

  let newUrl = `${ path }?` + order.reduce((paramList, id, idx) => {
    const { name, value } = data[id]

    if (idx === order.length - 1) {
      paramList += `${ name }=${ value }`
    } else {
      paramList += `${ name }=${ value }&`
    }

    return paramList
  }, ``)

  return await fetch(proxyUrl + newUrl, {
    method: 'POST',
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  })
}


class Login extends Component {
  constructor() {
    super()

    this.state = {
      login: '',
      password: '',
      error: '',
      remember: false,
    }
  }

  componentWillMount() {
    const login = localStorage.getItem('login')
    const password = localStorage.getItem('password')

    if (login && password) {
      this.setState({ login, password, remember: true })
    }
  }

  onRememberMe = ({ remember }) => this.setState({ remember })
  onChangeLogin = ({ value }) => this.setState({ login: value })
  onChangePassword = ({ value }) => this.setState({ password: value })
  onHandleClick = () => {
    const { login, password, remember } = this.state

    const { dispatch } = this.props

    if (remember) {
      localStorage.setItem('login', login)
      localStorage.setItem('password', password)
    }

    const data = {
      0: { name: 'email', value: login },
      1: { name: 'password', value: password }
    }

    const order = Object.keys(data)

    const result = api({ path: url, data, order })
      .then((responce) => {
        return responce.json()
      })
      .then((data) => {
        const { result, errors } = data

        const { code, message } = errors

        if (result && code === 0) {
          const { auth_token, full_name, position, periods } = result || {}

          const { monthly, quarter, annual } = periods

          const data = Object.keys(periods).reduce((data, item, index) => {
            return { ...data, [index]: { id: item, value: periods[item] } }
          }, {})

          dispatch({
            type: reducerLogin.actionMap.LOGIN,
            payload: {
              authToken: auth_token,
              userName: full_name,
              position
            }
          })

          dispatch({
            type: reducerPeriod.types.SET_PERIOD,
            payload: { data, order: Object.keys(data) }
          })

          go('/')
        }

        if (code === 1) {
          this.setState({ error: message })
        }
      })
  }

  render() {
    const { error, remember, login, password } = this.state

    return(
      <Wrapper>
        <WrapperContent>
          <Content>
            <Row>
              <BlockLogo />
            </Row>
            <Row>
              <BlockUserName onChange={ this.onChangeLogin } value={ login } />
              <BlockPassword onChange={ this.onChangePassword } value={ password } />
            </Row>
            <Row>
              <BlockRememberMe
                checked={ false }
                remember={ remember }
                onRemember={ this.onRememberMe }
              />
              <BlockSubmit onSubmit={ this.onHandleClick } />
            </Row>
            <Row>
              <Error>{ error }</Error>
            </Row>
          </Content>
        </WrapperContent>
      </Wrapper>
    )
  }
}

export default compose(
  connect()
  )(Login)