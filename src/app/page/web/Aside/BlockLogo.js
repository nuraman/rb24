import React                from 'react'

import styled               from 'styled-components'
import OrgStructure from "../PageOrgStructure/OrgStructure";


const Div = styled.div`
  display: flex;
  flex-direction: row;
  
  height: 82px;
  width: 100%;
  
  cursor: pointer;
  
  &:hover {
    background-color: #54adce;
  }
`

const DivIconWrapper = styled.div`
  display: flex;
  align-items: center;
  heigth: 100%;
  padding: 0px 30px 0px 40px;
`

const OrgStructureIcon = styled.div`
  min-width: 16px;
  width: 16px;
  height: 16px;
  
  background-image: url(assets/person.svg);
  background-size: cover;
`

const DivLabelWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

const Label = styled.label`
  font-family: RalewayLight;
  font-size: 20px;
  color: #fff;
`


export default () => {
  return(
    <Div>
    </Div>
  )
}