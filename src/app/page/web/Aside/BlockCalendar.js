import React                from 'react'

import styled               from 'styled-components'
import OrgStructure from "../PageOrgStructure/OrgStructure";
import Calendar from "../PageCalendar/Calendar";


const Div = styled.div`
  display: flex;
  flex-direction: row;
  
  height: 82px;
  width: 100%;
  
  cursor: pointer;
  
  &:hover {
    background-color: #54adce;
  }
`

const DivIconWrapper = styled.div`
  display: flex;
  align-items: center;
  heigth: 100%;
  padding: 0px 30px 0px 40px;
`

const CalendarIcon = styled.div`
  min-width: 17px;
  width: 17px;
  height: 16px;
  
  background-image: url(assets/calendar.svg);
  background-size: cover;
`

const DivLabelWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

const Label = styled.label`
  font-family: RalewaySemiBold;
  font-size: 16px;
  color: #fff;
  
  cursor: pointer;
`

const DivNumber = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;

  margin-left: 10px;

  border-radius: 50%;
  background-color: rgb(250, 125, 125);

  width: 19px;
  height: 19px;

  z-index: 15;
`
const LabelNumber = styled.label`
  font-family: RalewaySemiBold;
  font-size: 12px;
  color: #fff;
  
  height: 12px;
`

export default () => {
  return(
    <Div>
      <DivIconWrapper>
        <CalendarIcon />
      </DivIconWrapper>
      <DivLabelWrapper>
        <Label>
          КАЛЕНДАРЬ
        </Label>
        <DivNumber>
          <LabelNumber>
            2
          </LabelNumber>
        </DivNumber>
      </DivLabelWrapper>
    </Div>
  )
}