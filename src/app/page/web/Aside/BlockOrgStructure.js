import React                from 'react'

import styled               from 'styled-components'


import go                   from 'engine/history/go'


const Div = styled.div`
  display: flex;
  flex-direction: row;
  
  height: 82px;
  width: 100%;
  
  cursor: pointer;
  
  &:hover {
    background-color: #54adce;
  }
`

const DivIconWrapper = styled.div`
  display: flex;
  align-items: center;
  heigth: 100%;
  padding: 0px 30px 0px 40px;
`

const OrgStructureIcon = styled.div`
  min-width: 16px;
  width: 16px;
  height: 16px;
  
  background-image: url(assets/person.svg);
  background-size: cover;
`

const DivLabelWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

const Label = styled.label`
  font-family: RalewaySemiBold;
  font-size: 16px;
  color: #fff;
  
  cursor: pointer;
`


export default () => {
  const onHandleClick = () => {
    go('/OrgStructure')
  }

  return(
    <Div
      onClick={ onHandleClick }
    >
      <DivIconWrapper>
        <OrgStructureIcon />
      </DivIconWrapper>
      <DivLabelWrapper>
        <Label>
          ОРГСТРУКТУРА
        </Label>
      </DivLabelWrapper>
    </Div>
  )
}