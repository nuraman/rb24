import React                from 'react'

import styled               from 'styled-components'
import OrgStructure from "../PageOrgStructure/OrgStructure";


const Div = styled.div`
  display: flex;
  flex-direction: row;
  
  height: 82px;
  width: 100%;
  
  cursor: pointer;
  
  &:hover {
    background-color: #54adce;
  }
`

const Link = styled.a`
  text-decoration: none;
  margin-left: 90px;
  cursor: pointer;
`

const DivLabelWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

const Label = styled.label`
  font-family: RalewaySemiBold;
  font-size: 16px;
  color: #fff;
  
  cursor: pointer;
`


export default () => {
  return(
    <Div>
      <Link href='#'>
        <DivLabelWrapper>
          <Label>
            ВЫЙТИ
          </Label>
        </DivLabelWrapper>
      </Link>
    </Div>
  )
}