import React                from 'react'

import styled               from 'styled-components'
import OrgStructure from "../PageOrgStructure/OrgStructure";


const Div = styled.div`
  display: flex;
  flex-direction: row;
  
  height: 82px;
  width: 100%;
  
  cursor: pointer;
  
  &:hover {
    background-color: #54adce;
  }
`

const DivIconWrapper = styled.div`
  display: flex;
  align-items: center;
  heigth: 100%;
  padding: 0px 30px 0px 40px;
`

const SettingIcon = styled.div`
  min-width: 20px;
  width: 20px;
  height: 18px;
  
  background-image: url(assets/setting.svg);
  background-size: cover;
`

const DivLabelWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

const Label = styled.label`
  font-family: RalewaySemiBold;
  font-size: 16px;
  color: #fff;
  
  cursor: pointer;
`


export default () => {
  return(
    <Div>
      <DivIconWrapper>
        <SettingIcon />
      </DivIconWrapper>
      <DivLabelWrapper>
        <Label>
          НАСТРОЙКИ
        </Label>
      </DivLabelWrapper>
    </Div>
  )
}