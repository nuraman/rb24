import React, { Component } from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: block;
  width: 100%;
  height: 160px;
`

const DivAvatar = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  height: 80px;  
`

const Avatar = styled.div`
  border-radius: 50%;
  background-color: rgb(255, 255, 255);
  width: 58px;
  height: 58px;
  z-index: 26;
`

const DivLabel = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 80px;  
`

const LabelFIO = styled.div`
  font-family: RalewaySemiBold;
  font-size: 16px;
  color: #fff;
`

const LabelRole = styled.div`
  font-family: RalewaySemiBold;
  font-size: 14px;
  color: #b9d0da;
`

export default (props) => {
  const { userName, position } = props

  return(
    <Container>
      <DivAvatar>
        <Avatar />
      </DivAvatar>
      <DivLabel>
        <LabelFIO>
          { userName }
        </LabelFIO>
        <LabelRole>
          { position }
        </LabelRole>
      </DivLabel>
    </Container>
  )
}