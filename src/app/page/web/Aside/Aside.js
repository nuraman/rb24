import React, { Component } from 'react'

import { compose }          from 'redux'

import styled, { css }      from 'styled-components'


import BlockUserAvatar      from './BlockUserAvatar'
import BlockIndicator       from './BlockIndicator'
import BlockOrgStructure    from './BlockOrgStructure'
import BlockCalendar        from './BlockCalendar'
import BlockSetting         from './BlockSetting'
import BlockExit            from './BlockExit'

import withData             from 'app/page/web/header/hoc/withDataUser'


const Aside = styled.aside`
  width: 0px;
  height: 100%;
  position: absolute;
  top: 0px;
  left: 0px;
  overflow-x: hidden;
  overflow-y: auto;
  background-color: #70769b;
  transition: width 0.4s;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  
   &::-webkit-scrollbar
  {
    width: 9px;
  }

  &::-webkit-scrollbar-track
  {
    background: #73c7c5;
    background-clip: content-box;
  }

  &::-webkit-scrollbar-thumb
  {
    background: #484848;
    height: 35px;
    border-radius: 5px solid #484848;
    border: 1px solid #484848;
  }
 `

const AsideActive = Aside.extend`
  ${ props => props.active && css`
     width: 320px;
     transition: width 0.4s;
  `}
`

const AsideContent = styled.div`
  box-sizing: border-box;
  width: 320px;
  display: table;
`

const AsideFooter = styled.div`
  box-sizing: border-box;
  width: 320px;
  display: flex;
  padding: 20px;
  flex-direction: row;
  align-items: center;
`

const Logo = styled.div`
  width: 100px;
  height: 60px;
  
  padding-left: 140px;
  
  background: center no-repeat;
  background-image: url(assets/logo.png);
  background-size: 100px 60px;
`


class Menu extends Component {
  constructor() {
    super()

    this.state = {
      userName: '',
      position: ''
    }
  }

  componentWillMount() {
    const { userName, position } = this.props

    this.setState({ userName, position })
  }

  render() {
    const { userName, position } = this.state

    const { open } = this.props

    return (
      <AsideActive active={ open }>
        <AsideContent>
          <BlockUserAvatar
            userName={ userName }
            position={ position }
          />
          <BlockIndicator />
          <BlockOrgStructure />
          <BlockCalendar />
          <BlockSetting />
          <BlockExit />
        </AsideContent>
        <AsideFooter>
          <Logo />
        </AsideFooter>
      </AsideActive>
    )
  }
}

export default compose(
  withData,
)(Menu)