import React                from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: flex;
  flex-direction: row;
   
  margin-right: 20px;
`

const BellIcon = styled.div`
  width: 45px;
  height: 45px;
  
  background: center no-repeat;
  background-image: url(assets/bellIcon.png);
  background-size: 45px 45px;
`

const CircleIcon = styled.div`
  min-width: 5px;
  width: 5px;
  height: 5px;
  
  margin-left: -15px;
  margin-top: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/circleRed.svg);
  background-size: cover;
`

export default (props) => {
  return (
    <Container>
      <BellIcon/>
      <CircleIcon/>
    </Container>
  )
}