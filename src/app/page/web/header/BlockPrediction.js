import React, { Component } from 'react'

import { compose }          from 'redux'
import { connect }          from 'react-redux'

import styled               from 'styled-components'


import PredictionList       from './PredictionList'
import withPeriod           from 'app/page/web/header/hoc/withDataPeriod'
import reducer              from './reducer'


const DropdownContent = styled.div`
  position: absolute;
  display: none;
  width: 100px;
  height: 90px;
  
  z-index: 500;
  
  background-color: #fff;
  
  box-shadow: 0px 2px 20px 0px rgba(19, 92, 99, 0.16);
`

const Dropdown = styled.div`
  position: relative;
  
  &:hover ${ DropdownContent }{
    display: block;
  }
`

const Container = styled.div`
  position: relative;

  display: flex;
  flex-direction: row;
  align-items: center;
  
  height: 40px;
  width: 200px;
  padding-left: 20px;
`

const LabelColor = styled.div`
  font-family: RalewayLight;
  font-size: 16px;
  color: #7e7e7f;
`

const LabelSelect = styled.div`
  font-family: RalewayRegular
  font-weight: bold;
  font-size: 16px;
  color: #000;
  
  margin-left: 5px;
  
  cursor: pointer;
  
  border-bottom: 1px dotted #000;
`

const DownIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 5px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/down.svg);
  background-size: cover;
`

const Button = styled.button`
  text-decoration: none;
`

const DivSelect = styled.div`
  height: 20px;
  width: 100%;

  padding: 5px 0px 5px 0px;
  
  display: flex;
  flex-direction: row;
  align-items: center;
  
  cursor: pointer;
  
  &:hover {
    background-color: #dae6ec;
  }
`

const Label = styled.label`
  font-family: RalewayLight;
  font-size: 14px;
  color: #000;
  
  cursor: pointer;
  
  padding-left: 20px;
`

const LabelSelected = styled.label`
  font-family: RalewayLight;
  font-size: 14px;
  font-weight: bold;
  color: #000;
  
  cursor: pointer;
`

const CircleIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 9px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/circleBlue.svg);
  background-size: cover
`


class BlockPrediction extends Component {
  constructor() {
    super()

    this.state = {
      selectedId: '',
      selectedValue: '',
    }
  }

  componentWillMount() {
    const { data, order = [], dispatch } = this.props

    if (order.length > 0) {
      const { id, value } = data[order[0]]

      this.setState({ data, order, selectedId: id, selectedValue: value })

      dispatch({
        type: reducer.types.SET_SELECTED,
        payload: { id }
      })
    }
  }

  onSelect = ({ id, value }) => {
    this.setState({ selectedId: id, selectedValue: value })

    const { dispatch } = this.props

    dispatch({
      type: reducer.types.SET_SELECTED,
      payload: { id }
    })
  }

  render() {
   const { data, order = [], selectedId, selectedValue } = this.state


    return (
      <Container>
        <LabelColor> Прогноз на </LabelColor>
        <LabelSelect>
          { selectedValue }
        </LabelSelect>
        <Dropdown>
          <DownIcon />
          <DropdownContent>
            {
              order.map((item) => {
                const { id, value } = data[item]

                if (selectedId == id) {
                  return (
                    <DivSelect
                      key={ id }
                      onClick={ () => this.onSelect({ id, value }) }
                    >
                      <CircleIcon />
                      <LabelSelected>
                        { value }
                      </LabelSelected>
                    </DivSelect>
                  )
                }

                return (
                  <DivSelect
                    key={ id }
                    onClick={ () => this.onSelect({ id, value }) }
                  >
                    <Label>
                      { value }
                    </Label>
                  </DivSelect>
                )
              })
            }
          </DropdownContent>
        </Dropdown>
      </Container>
      )
  }
}

export default compose(
  connect(),
  withPeriod
)(BlockPrediction)
