import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'


import EmployeeList         from './EmployeeList'
import fetchKPI             from '../api/fetchKPI'
import api                  from '../api/api'
import reducer              from './reducer'


const DropdownContent = styled.div`
  position: absolute;
  display: none;
  width: 100%;
  
  z-index: 500;
  
  background-color: #fff;
  
  box-shadow: 0px 2px 20px 0px rgba(19, 92, 99, 0.16);
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  
  &:hover ${ DropdownContent } {
    display: block;
  }
`

const Dropdown = styled.div`
  position: relative;
  display: block;
  margin-left: 10px;
`

const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  height: 40px;
  width: 380px;
  padding-left: 20px;
`

const Label = styled.label`
  font-family: RalewayRegular;
  font-weight: bold;
  font-size: 16px;
  color: #000;
  
  cursor: pointer;
  
  border-bottom: 1px dotted #000;
`

const DownIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 5px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/down.svg);
  background-size: cover;
`

class BlockEmployee extends Component {
  constructor() {
    super()

    this.state = {
      userName: '',
      position: '',
      employee: {},

      employeeId: ''
    }
  }

  componentWillMount() {
    const { userName, position } = this.props

    this.setState({ userName, position })
  }

  componentWillReceiveProps(nextProps) {
    const { employee = {}, authToken, selectedPeriod, dispatch } = nextProps

    const { employeeId } = this.state

    this.setState({ employee })

    if (employeeId && selectedPeriod) {
      fetchKPI({ authToken, employeeId, period: selectedPeriod })
        .then((responce) => {
          return responce.json()
        })
        .then((data) => {
          const { result, errors } = data

          if (result && !errors.code) {
            const dataServer = api.getKPI({ result })
            const orderServer = Object.keys(dataServer)

            dispatch({
              type: reducer.types.SET_KPI,
              payload: { newData: dataServer, newOrder: orderServer }
            })
          }
        })
    }
  }

  onSelectEmployee = ({ id }) => {
    const { authToken, selectedPeriod } = this.props

    const { dispatch } = this.props

    fetchKPI({ authToken, employeeId: id, period: selectedPeriod })
      .then((responce) => {
        return responce.json()
      })
      .then((data) => {
        const { result, errors } = data

        if (result && !errors.code) {
          const dataServer = api.getKPI({ result })
          const orderServer = Object.keys(dataServer)

          dispatch({
            type: reducer.types.SET_KPI,
            payload: { newData: dataServer, newOrder: orderServer }
          })
        }
    })

    this.setState({ employeeId: id })
  }

  render() {
    const { userName, position, employee } = this.state

    return (
      <Wrapper>
        <Container>
          <Label onClick={ this.onHandleClick }>
            { `${ userName } (${ position })` }
          </Label>
          <DownIcon/>
        </Container>
        <Dropdown>
          <DropdownContent>
            <EmployeeList employee={ employee } onSelectEmployee={ this.onSelectEmployee } />
          </DropdownContent>
        </Dropdown>
      </Wrapper>
    )
  }
}

const mapStateToProps = (state) => {
  const { login: { userLogin }, period } = state
  const { data, order } = userLogin || {}

  const { authToken, userName, position } = data[order[0]] || {}

  const { selected } = period

  return {
    authToken,
    userName,
    position,
    selectedPeriod: selected
  }
}

export default compose(
  connect(mapStateToProps)
)(BlockEmployee)