import React, { Component } from 'react'

import styled               from 'styled-components'


import BlockChildren        from './Children'


const Wrapper = styled.div`
  display: block;
  padding-left: 10px;
`

const Content = styled.div`
  display: block;
  padding-left: 10px;
`

const CircleIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 9px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/circleBlue.svg);
  background-size: cover
`

const PositiveIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 9px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/positive.svg);
  background-size: cover
`

const NegativeIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 2px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/negative.svg);
  background-size: cover
`

const DivItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  padding-left: 5px;
  
  width: 100%;
  height: 30px;
`

const DivMainLabel = styled.div`
  font-family: RalewayBold;
  font-size: 16px;
  color: #000;
  
  cursor: pointer;
  
  padding-left: 5px;
`

const DivLabel = styled.div`
  font-family: RalewayLight;
  font-size: 16px;
  color: #535353;
  
  padding-left: 5px;
  
  cursor: pointer;
  
  &:hover {
    color: #2096b7;
  }  
`


class EmployeeList extends Component {
  constructor() {
    super()

    this.state = {
      parentId: '',
      parentName: '',
      children: [],

      openParentChild: '',
    }
  }

  componentWillMount() {
    const { employee } = this.props

    const { id, fullName, children } = employee[Object.keys(employee)[0]] || {}

    this.setState({ employee, parentId: id, parentName: fullName, children })
  }

  componentWillReceiveProps(nextProps) {
    const { employee } = nextProps || {}

    const { id, fullName, children } = employee[Object.keys(employee)[0]] || {}

    this.setState({ employee, parentId: id, parentName: fullName, children })
  }

  onHandleClickShow = ({ parentId }) => this.setState({ openParentChild: parentId })

  onSelect = ({ id }) => {
    this.setState({ selectId: id })

    const { onSelectEmployee } = this.props

    onSelectEmployee({ id })
  }

  onHandleClickLabel = ({ id }) => {
    const { onSelectEmployee } = this.props

    onSelectEmployee({ id })
  }

  render() {
    const { parentId, parentName, children = [], openParentChild } = this.state

    return (
      <Wrapper>
        <DivItem>
          <CircleIcon />
          <DivMainLabel>
            { parentName }
          </DivMainLabel>
        </DivItem>
        <Content>
          {
            children.map((item, index) => {
              const { id, fullName, children } = item[Object.keys(item)[0]]

              if (children.length > 0) {
                return (
                  <div key={ index }>
                    <DivItem>
                      {
                        openParentChild == id ?
                          <NegativeIcon onClick={() => this.onHandleClickShow({ parentId: '' })}/> :
                          <PositiveIcon onClick={() => this.onHandleClickShow({ parentId: id })}/>
                      }
                      <DivLabel onClick={ () => this.onHandleClickLabel({ id }) }>
                        { fullName }
                      </DivLabel>
                    </DivItem>
                    {
                      openParentChild == id ? <BlockChildren children={ children } onSelect={ this.onSelect } /> : null
                    }
                  </div>
                )
              }

              if (!children.length) {
                return (
                  <DivItem key={ index }>
                    <NegativeIcon />
                    <DivLabel onClick={ () => this.onHandleClickLabel({ id }) }>
                      { fullName }
                    </DivLabel>
                  </DivItem>
                )
              }
            })
          }
        </Content>
      </Wrapper>
    )
  }
}

export default EmployeeList