const name = `kpi`

const types = {
  SET_KPI: `${ name }/SET_KPI`
}

const STATE = {
  data: {},
  order: []
}

const reducer = (state=STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case types.SET_KPI: {
      const { newData, newOrder } = payload

      return {
        ...state,
        data: newData,
        order: newOrder,
      }
    }
    default: {
      return state
    }
  }
}

export default {
  reducer,
  types
}