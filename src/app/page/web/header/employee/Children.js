import React, { Component } from 'react'

import styled               from 'styled-components'

import BlockChildren        from './Children'


const Wrapper = styled.div`
  display: block;
  padding-left: 18px;
  padding-right: 5px;
`

const DivItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 100%;
  height: 30px;
  
  margin-bottom: 10px;
`

const PositiveIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 9px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/positive.svg);
  background-size: cover
`

const NegativeIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 2px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/negative.svg);
  background-size: cover
`

const DivLabel = styled.div`
  font-family: RalewayLight;
  font-size: 16px;
  color: #535353;
  
  cursor: pointer;
  
  padding-left: 5px;
  
  &:hover {
    color: #2096b7;
  }  
`


class Children extends Component {
  constructor() {
    super()

    this.state = {
      children: [],
      openParentChild: ''
    }
  }

  componentWillMount() {
    const { children } = this.props

    this.setState({ children })
  }

  componentWillReceiveProps(nextProps) {
    const { children } = this.props

    this.setState({ children })
  }

  onHandleClickShow = ({ parentId }) => this.setState({ openParentChild: parentId })

  onHandleClickLabel = ({ id }) => {
    const { onSelect } = this.props

    onSelect({ id })
  }

  onSelect = ({ id }) => {
    const { onSelect } = this.props

    onSelect({ id })
  }

  render() {
    const { children = [], openParentChild } = this.state

    return (
      <Wrapper>
        {
          children.map((item, index) => {
            const { id, fullName, children } = item[Object.keys(item)[0]]

            if (children.length > 0) return (
              <div key={ index }>
                <DivItem>
                  {
                    openParentChild == id ?
                      <NegativeIcon onClick={ () => this.onHandleClickShow({ parentId: '' }) } /> :
                      <PositiveIcon onClick={ () => this.onHandleClickShow({ parentId: id })} />
                  }
                  <DivLabel onClick={ () => this.onHandleClickLabel({ id }) }>
                    { fullName }
                  </DivLabel>
                </DivItem>
                { openParentChild == id ? <BlockChildren children={ children }  onSelect={ this.onSelect } /> : null }
              </div>
            )

            if (!children.length) {
              return (
                <DivItem key={ index }>
                  <NegativeIcon />
                  <DivLabel onClick={ () => this.onHandleClickLabel({ id }) }>
                    { fullName }
                  </DivLabel>
                </DivItem>
              )
            }
          })
        }
      </Wrapper>
    )
  }
}

export default Children