import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


function withData(ComposedComponent) {
  function decorator(props) {
    return (
      <ComposedComponent { ...props } />
    )
  }

  function mapStateToProps(state) {
    const { period } = state
    const { data, order } = period || {}

    return {
      data,
      order,
    }
  }

  return compose(
    connect(mapStateToProps)
  )(decorator)
}

export default withData