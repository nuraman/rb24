import React                from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'


function withData(ComposedComponent) {
  function decorator(props) {
    return (
      <ComposedComponent { ...props } />
    )
  }

  function mapStateToProps(state) {
    const { login: { userLogin } } = state
    const { data, order } = userLogin || {}

    const { authToken, userName, position } = data[order[0]] || {}

    return {
      authToken,
      userName,
      position
    }
  }

  return compose(
    connect(mapStateToProps)
  )(decorator)
}

export default withData