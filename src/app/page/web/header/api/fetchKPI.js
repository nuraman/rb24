const url = `https://rb24.herokuapp.com/kpi_instances`
const proxy = `https://cors-anywhere.herokuapp.com/`


const kpiList = async (payload) => {
  const { authToken, employeeId, period } = payload
  const urlWithParameter = `${ url }?employee_id=${ employeeId }&period=${ period }`

  return await fetch(proxy + urlWithParameter, {
    method: 'GET',
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token token=${ authToken }`
    }
  })
}

export default kpiList