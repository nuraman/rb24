const url = `https://rb24.herokuapp.com/employees`
const proxy = `https://cors-anywhere.herokuapp.com/`


const employee = async (payload) => {
  const { authToken } = payload

  return await fetch(proxy + url, {
    method: 'GET',
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token token=${ authToken }`
    }
  })
}

export default employee