import _                    from 'lodash'

function getEmployee ({ result }) {
  let data = {}

  var recursive = function Rec({ result, parent = [] }) {
    const { employee_id, fullname, children = [] } = result || {}
    const id = employee_id
    if(!Object.keys(data).length) { //смотрим родительский элемент это
      data =  { [id]: { id, fullName: fullname, children: [] } }

      if (children.length > 0) {
        children.forEach((child) => {
          Rec({ result: child, parent: data[id].children })
        })
      }
    } else {//смотрим дочерние элементы
      const { employee_id, fullname, children = []  } = result

      const dataTemp = { [employee_id]: { id: employee_id, fullName: fullname, children: [] } }

      parent.push(dataTemp)

      if(children.length > 0) {
        children.forEach((child) => {
          Rec({ result: child, parent: dataTemp[employee_id].children })
        })
      }
    }
  }

  recursive({ result })

  return data
}

function getKPI ({ result }) {
  let data = {}

  var recursive = function Rec({ child = [], parent }) {
    child.forEach((item, index) => {
      const {
        kpi_id,
        kpi_type,
        title,
        the_more_is_better,
        trend,
        plan,
        fact,
        execution,
        completed,
        measure,
        kpi_salary,
        ordinal_number,
        children = []
      } = item

      if (children.length > 0) {
        if (parent) {
          _.merge(data, {
            [kpi_id]: {
              id: kpi_id,
              kpiType: kpi_type,
              title: title,
              theMoreIsBetter: the_more_is_better,
              trend,
              plan,
              fact,
              execution,
              completed,
              measure,
              kpiSalay: kpi_salary,
              ordinalNumber: ordinal_number,
              parent
            }
          })
        } else {
          _.merge(data, {
            [kpi_id]: {
              id: kpi_id,
              kpiType: kpi_type,
              title: title,
              theMoreIsBetter: the_more_is_better,
              trend,
              plan,
              fact,
              execution,
              completed,
              measure,
              kpiSalay: kpi_salary,
              ordinalNumber: ordinal_number,
              parent: null,
            }
          })
        }

        Rec({ child: children, parent: kpi_id })
      }

      if (!children.length && parent) {
        _.merge(data, {
          [kpi_id]: {
            id: kpi_id,
            kpiType: kpi_type,
            title: title,
            theMoreIsBetter: the_more_is_better,
            trend,
            plan,
            fact,
            execution,
            completed,
            measure,
            kpiSalay: kpi_salary,
            ordinalNumber: ordinal_number,
            parent
          }
        })
      }

      if (!children.length && !parent) {
        _.merge(data, {
          [kpi_id]: {
            id: kpi_id,
            kpiType: kpi_type,
            title: title,
            theMoreIsBetter: the_more_is_better,
            trend,
            plan,
            fact,
            execution,
            completed,
            measure,
            kpiSalay: kpi_salary,
            ordinalNumber: ordinal_number,
            parent: null,
          }
        })
      }
    })
   }

  recursive({ child: result })

  return data
}


export default {
  getEmployee,
  getKPI
}