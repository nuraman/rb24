import React, { Component } from 'react'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import styled               from 'styled-components'


import withData             from './hoc/withDataUser'
import BlockNavigator       from './Navigator'
import BlockWorker          from './employee/Container'
import BlockPrediction      from './BlockPrediction'
import BlockKPI             from './BlockKPI'
import BlockBell            from './BlockBell'

import fetchData            from './api/fetchEmployee'
import api                  from './api/api'


const Navbar = styled.div`
  position: relative;
  
  height: 50px;
  
  width: ${ props => props.shift ? 'calc(100% - 320px)' : '100%' };
  margin-left: ${ props => props.shift ? '320px' : '0px' };
  
  display: flex;
  flex-direction: row;
  align-items: center;
`

const Flex = styled.div`
  display: flex;
  flex: 1;
`

const SearchIcon = styled.div`
  min-width: 24px;
  width: 24px;
  height: 18px;
  
  margin-left: 5px;
  
  cursor: pointer;
  
  background-image: url(assets/search.svg);
  background-size: cover;
`

class Header extends Component {
  constructor() {
    super()

    this.state = {
      open: false,
      employee: {},
    }
  }

  onClickNavigator = () => {
    const { openAside } = this.props

    const { open } = this.state

    openAside({ open: !open })

    this.setState({ open: !open })
  }

  componentWillMount() {
    const { authToken } = this.props

    if (authToken) {
      const result = fetchData({ authToken })
        .then((responce) => {
          return responce.json()
        })
        .then((data) => {
          const { result, errors } = data

          if (result && errors.code == 0) {
            const employee = api.getEmployee({ result })

            if (Object.keys(employee).length > 0) this.setState({ employee })
          }
        })
        .catch((e) => {
          console.log(e)
        })
    }
  }


  render() {
    const { predictionId, open, employee } = this.state

    return (
      <Navbar shift={ open }>
        <BlockNavigator onClickNavigator={ this.onClickNavigator  }/>
        <BlockWorker employee={ employee } />
        <BlockPrediction />
        <Flex />
        <BlockKPI />
        <SearchIcon />
        <BlockBell />
      </Navbar>
    )
  }
}

export default compose(
  connect(),
  withData,
)(Header)