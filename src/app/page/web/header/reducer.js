const name = `header`

const STATE = {
  data: {},
  order: [],
  selected: ''
}

const types = {
  SET_PERIOD: `${ name }/SET_PERIOD`,
  SET_SELECTED: `${ name }/SET_SELECTED`
}

const reducer = (state = STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case types.SET_PERIOD: {
      const { data, order } = payload

      return {
        ...state,
        data: data,
        order: order,
      }
    }
    case types.SET_SELECTED: {
      const { id } = payload

      return {
        ...state,
        selected: id,
      }
    }
    default: {
      return state
    }
  }
}

export default {
  reducer,
  types
}