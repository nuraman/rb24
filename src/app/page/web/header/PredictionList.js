import React                from 'react'

import styled               from 'styled-components'


const DivOptions = styled.div`
  position: absolute;
  top: ${ props => props.top + 10 }px;
  left: ${ props => props.left - 70 }px;
  
  width: 100px;
  height: 90px;
  
  z-index: 999;
  
  background-color: #fff;
  
  box-shadow: 0px 2px 20px 0px rgba(19, 92, 99, 0.16);
`

const DivSelect = styled.div`
  height: 20px;
  width: 100%;

  padding: 5px 0px 5px 0px;
  
  display: flex;
  flex-direction: row;
  align-items: center;
  
  cursor: pointer;
  
  &:hover {
    background-color: #dae6ec;
  }
`

const Label = styled.label`
  font-family: RalewayLight;
  font-size: 14px;
  color: #000;
  
  cursor: pointer;
  
  padding-left: 20px;
`

const LabelSelected = styled.label`
  font-family: RalewayLight;
  font-size: 14px;
  font-weight: bold;
  color: #000;
  
  cursor: pointer;
`

const CircleIcon = styled.div`
  min-width: 10px;
  width: 10px;
  height: 9px;
  
  margin-left: 10px;
  
  cursor: pointer;
  
  background-image: url(assets/circleBlue.svg);
  background-size: cover
`

const period = {
  0: 'Сегодня',
  1: 'Месяц',
  2: 'Квартал'
}

export default (props) => {
  const { open, top, left, predictionId } = props

  const order = Object.keys(period)

  if (!open) return null

  const onSelect = (id) => {
    const { onSelectPeriod } = props

    onSelectPeriod(id)
  }

  return (
    <DivOptions top={ top } left={ left }>
      {
        order.map((id) => {
          const name = period[id]

          if (id == predictionId) {
            return (
              <DivSelect
                key={ id }
                onClick={ () => onSelect(id) }
              >
                <CircleIcon />
                <LabelSelected>
                  { name }
                </LabelSelected>
              </DivSelect>
            )
          }

          return (
            <DivSelect
              key={ id }
              onClick={ () => onSelect(id) }
            >
              <Label>
                { name }
              </Label>
            </DivSelect>
          )
        })
      }
    </DivOptions>
  )
}
