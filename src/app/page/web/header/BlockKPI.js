import React, { Component } from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  width: 250px;
`

const LabelKPI = styled.div`
  font-family: ${ props => props.turnOn? 'RalewayBold' : 'RalewayLight'};
  font-size:16px;
  color: ${ props => props.turnOn ? '#000' : '#7e7e7f' };
  
  border-bottom: ${ props => props.turnOn ? '' : '1px dotted #7e7e7f' };
`

const Label = styled.div`
  font-family: ${ props => props.turnOn? 'RalewayLight' : 'RalewayBold'};
  font-size: 14px;
  color: ${ props => props.turnOn ? '#7e7e7f' : '#000' };
  
  border-bottom: ${ props => props.turnOn ? '1px dotted #7e7e7f' : '' };
`

const CheckboxWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 1px 3px 0px rgba(41, 57, 63, 0.11);
  
  border-radius: 25px;
  
  width: 54px;
  height: 24px;
  
  margin: 0px 10px 0px 10px;
  
  z-index: 158;
`

const CircleIcon = styled.div`
  min-width: 24px;
  width: 24px;
  height: 18px;
  
  margin-left: 5px;
  
  margin-left: ${ props =>props.turnOn ? '' : 'auto' };
  
  cursor: pointer;
  
  background-image: url(assets/circleCheckbox.svg);
  background-size: cover;
`


class BlockKPI extends Component {
  constructor() {
    super()

    this.state = {
      turnOn: true
    }
  }

  onChangeCheckbox = () => {
    const { turnOn } = this.state

    if (turnOn) {
      this.setState({ turnOn: false })
    }

    if (!turnOn) {
      this.setState({ turnOn: true })
    }
  }

  render() {
    const { turnOn } = this.state

    return (
      <Container>
        <LabelKPI turnOn={ turnOn }>
          KPI
        </LabelKPI>
        <CheckboxWrapper
          onClick={ this.onChangeCheckbox }
        >
          <CircleIcon
            turnOn={ turnOn }

            onClick={ this.onChangeCheckbox }
          />
        </CheckboxWrapper>
        <Label turnOn={ turnOn }>
          Заработная плата
        </Label>
      </Container>
    )
  }
}

export default BlockKPI