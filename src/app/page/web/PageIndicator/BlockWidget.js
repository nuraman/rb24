import uuid                 from 'uuid'

import React, { Component } from 'react'

import styled               from 'styled-components'

import BlockColumn          from './BlockColumn'


const WrapperContent = styled.div`
  display: block;
  padding: 20px;
  width: calc(100% - 40px);
  overflow: auto;
`

const Row = styled.div`
  display: flex; 
  flex-direction: row;
`

const Column = styled.div`
  display: flex;
  flex-direction: column;
`

class BlockWidget extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      order: [],
      selectId: '',

      /*
      parents, parentMoreThan: [{ data: {}, order: [] }, { data: {}, order: [] }]
       */
      parentList: [],
      parentMoreThan: [],
    }
  }

  componentWillMount() {
    const { data, order } = this.props

    const parentList = this.getKpiParent({ data, order })

    const parentMoreThan = this.getDataMoreThen({ parentList })

    this.setState({ data, order, parentList, parentMoreThan })
  }

  componentWillReceiveProps(nextProps) {
    const { data, order } = nextProps

    const parentList = this.getKpiParent({ data, order })

    const parentMoreThan = this.getDataMoreThen({ parentList })

    this.setState({ data, order, parentList, parentMoreThan })
  }

  getKpiParent = ({ data, order }) => {
    return order.reduce((parentList, item) => {
      const { parent } = data[item]

      if (!parent) {
        parentList.push({ ...data[item] })
      }

      return parentList
    }, [])
  }

  getDataMoreThen = ({ parentList }) => {
    const widgetDataList = []
    if (parentList.length > 3) {
      let countRow = parentList.length / 3

      let lastIndex = 0;
      for (let i = 0; i < countRow; i++) {
        widgetDataList.push(parentList.slice(lastIndex, lastIndex + 3))
        lastIndex += 3
      }
    } else {
      widgetDataList.push(parentList.slice(0, 3))
    }

    return widgetDataList
  }

  render() {
    const { data, order, parentMoreThan } = this.state

    return (
      <WrapperContent>
        {
          parentMoreThan.map((item, index) => {
            return (
              <Row key={ index }>
              {
                item.map((el, index) => {
                  return (
                    <BlockColumn
                      key={ index }
                      dataParent={ el }
                      data={ data }
                      order={ order }
                    />
                  )
                })
              }
            </Row>)
          })
        }
      </WrapperContent>
    )
  }
}

export default BlockWidget