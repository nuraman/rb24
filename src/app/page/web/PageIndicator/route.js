import React, { Component } from 'react'


import Indicator            from './Container'


const title = 'Indicator'

export default {
  path: '/',

  action: () => {
    return {
      title,
      component: (<Indicator />)
    }
  }
}