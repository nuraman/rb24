import React, { Component } from 'react'

import styled               from 'styled-components'

import WidgetParent         from 'app/widget/web/GoodExchange/Container'
import BlockKpi             from 'app/widget/web/Branch/Container'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  position: relative;
  padding: 0 5px;
  width: 420px;
`

const WrapperSecondWidget = styled.div`
  position: absolute;
  display: ${ props => props.display ? 'flex' : 'none' };
  top: ${ props=>props.top }px;
  margin-top: 10px
  z-index: 200;
`

const WrapperThirdWidget = styled.div`
  position: absolute;
  display: ${ props => props.display ? 'flex' : 'none' };
  top: ${ props=>props.top }px;
  margin-top: 20px
  z-index: 200;
`

class BlockColumn extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      order: [],

      dataParent: {},
      dataSecondList: [],
      dataThirdList: [],

      showSecond: '',
      showThird: '',

      height: 350,
    }
  }

  componentWillMount() {
    const { data, order, dataParent } = this.props

    this.setState({ data, order, dataParent })
  }

  componentWillReceiveProps(nextProps) {
    const { data, order, dataParent } = nextProps

    this.setState({ data, order, dataParent })
  }

  onShowSecondWidget = ({ id }) => {
    const { data, order, dataSecondList, showSecond } = this.state

    if (!showSecond) {
      const children = order.reduce((children, item, index) => {
        const { parent } = data[item]

        if (parent == id) {
          children.push(data[item])
        }

        return [...children]
      }, [])

      return this.setState({
        showSecond: children.length > 0 ? 'true' : '',
        dataSecondList: children
      })
    }

    if (showSecond) {
      this.setState({
        showSecond: '',
        showThird: '',
        dataThirdList: [],
        dataSecondList: []
      })
    }
  }

  onShowThirdWidget = ({ id }) => {
    const { data, order, dataThirdList, showThird } = this.state

    if (!showThird) {
      const children = order.reduce((children, item, index) => {
        const { parent } = data[item]

        if (parent == id) {
          children.push(data[item])
        }

        return [...children]
      }, [])

      return this.setState({
        showThird: children.length > 0 ? 'true' : '',
        dataThirdList: children
      })
    }

    if (showThird) {
      this.setState({ showThird: '', dataThirdList: [] })
    }
  }

  render() {
    const { dataParent, dataSecondList, dataThirdList, showSecond, showThird, height } = this.state

    return (
      <Container>
        <WidgetParent { ...dataParent } onSelect={ this.onShowSecondWidget } />
        <WrapperSecondWidget display={ showSecond } top={ height }>
          <BlockKpi dataList={ dataSecondList } onSelect={ this.onShowThirdWidget } />
        </WrapperSecondWidget>
        <WrapperThirdWidget display={ showThird } top={ height * 2 }>
          <BlockKpi dataList={ dataThirdList } />
        </WrapperThirdWidget>
      </Container>
    )
  }

}

export default BlockColumn