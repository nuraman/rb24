import React, { Component } from 'react'

import styled               from 'styled-components'

import { connect }          from 'react-redux'
import { compose }          from 'redux'

import go                   from 'engine/history/go'


import BlockWidget          from './BlockWidget'


const Page = styled.div`
  width: 100%;
  height: calc(100% - 50px);
  display: flex;
  flex-direction: row;
`

const WrapperContent = styled.div`
  display: block;
  padding: 20px;
`

const Row = styled.div`
  display: flex; 
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`

class Indicator extends Component {
  constructor() {
    super()

    this.state = {
      data: {},
      order: []
    }
  }

  componentWillMount() {
    const { isAuth, authToken, data = {}, order = [] } = this.props

    if (!isAuth) return go('/Login')

/*    const dataServer = {
      [0]: { id: 1, kpiType: 'planned', title: 'Товарооборот', trend: 'up', plan: 345, fact: 200, execution: 40, completed: false, kpiSalay:true, measure: 'руб.', parent: null },
      [1]: { id: 2, kpiType: 'planned', title: 'Дебиторская задолжность', trend: 'down', plan: 345, fact: 200, execution: 100, completed: false, kpiSalay:true, measure: '%', parent: null },
      [3]: { id: 3, kpiType: 'planned', title: 'Оценка удовлетворенности', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:true, measure: '', parent: null },
      [4]: { id: 4, kpiType: 'planned', title: 'Количество контрагенов', trend: 'down', plan: 900, fact: 900, execution: 100, completed: true, kpiSalay:true, measure: '', parent: null },
      [5]: { id: 5, kpiType: 'normalized', title: 'Мржинальность', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: null },

      [6]: { id: 6, kpiType: 'planned', title: 'Товарооборот1', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: 1 },
      [7]: { id: 7, kpiType: 'planned', title: 'Дебиторская задолженность 1', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: 2 },
      [8]: { id: 8, kpiType: 'planned', title: 'Оценка удовлетворенности1', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: 3 },
      [11]: { id: 11, kpiType: 'planned', title: 'Товарооборот11', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: 1 },
      [12]: { id: 12, kpiType: 'planned', title: 'Товарооборот12', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: 1 },


      [9]: { id: 9, kpiType: 'planned', title: 'Товарооборот2', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: 6 },
      [10]: { id: 10, kpiType: 'planned', title: 'Дебиторская задолженность 2', trend: 'up', plan: 345, fact: 200, execution: 4.6, completed: false, kpiSalay:false, measure: '', parent: 7 },
    }

    const orderServer = Object.keys(dataServer)
*/
    this.setState({ data, order })
  }

  componentWillReceiveProps(nextProps) {
    const { data = {}, order = [] } = nextProps

    this.setState({ data, order })
  }

  render() {
    const { data, order } = this.state

    if (!order.length) {
      return <Page />
    }

    let countRow = order.length / 3

    return (
      <Page>
       <BlockWidget data={ data } order={ order } />
      </Page>
    )
  }
}

const mapStateToProps = state => {
  const { login: { userLogin }, kpi } = state

  const { order, data } = userLogin

  const id = order[0]

  const { authToken } = data[id] || {}

  return {
    isAuth: order.length > 0,
    authToken,
    data: kpi.data,
    order: kpi.order
  }
}

export default compose(
  connect(mapStateToProps)
)(Indicator)

