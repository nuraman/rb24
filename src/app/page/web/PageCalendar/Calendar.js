import React, { Component } from 'react'


import Header               from 'app/page/web/header/Header'
import PredictionList       from 'app/page/web/header/PredictionList'


class Calendar extends Component {
  constructor() {
    super()

    this.state = {
      predictionId: '',
      openPrediction: false,
      topPrediction: 0,
      leftPrediction: 0,
    }
  }

  onOpenPrediction = ({ top, left }) => {
    this.setState({
      leftPrediction: left,
      openPrediction: !this.state.openPrediction,
      topPrediction: top,
    })
  }

  onSelectPeriodPrediction = (id) => {
    this.setState({ predictionId: id, openPrediction: !this.state.openPrediction })
  }

  render() {
    const { leftPrediction, topPrediction, openPrediction, predictionId } = this.state

    return (
      <div>
        <Header
          onOpenPrediction={ this.onOpenPrediction }
          predictionId={ predictionId  }
        />
        <PredictionList
          left={ leftPrediction }
          open={ openPrediction }
          top={ topPrediction }
          predictionId={ predictionId }

          onSelectPeriod={ this.onSelectPeriodPrediction }
        />
      </div>
    )
  }
}

export default Calendar