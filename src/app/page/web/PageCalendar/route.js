import React                from 'react'


import Calendar             from './Calendar'


const title = 'Calendar'

export default {
  path: '/Calendar',
  action: () => {
    return {
      title,
      component: (<Calendar />)
    }
  }
}