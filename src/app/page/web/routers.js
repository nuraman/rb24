import routeCalendar        from 'app/page/web/PageCalendar/route'
import routeIndicator       from 'app/page/web/PageIndicator/route'
import routeOrgStructure    from 'app/page/web/PageOrgStructure/route'
import routeLogin           from 'app/page/web/PageLogin/route'


export default  {
  path: '/',

  children: [
    routeCalendar,
    routeIndicator,
    routeOrgStructure,
    routeLogin
  ],

  async action({ next }) {
    const route = await next()

    route.title = `${ route.title }`
    route.description = ``

    return route
  }
}
