import React, { Component } from 'react'


import OrgStructure         from './OrgStructure'


const title = 'OrgStructure'

export default {
  path: '/OrgStructure',
  action: () => {
    return {
      title,
      component: (<OrgStructure />)
    }
  }
}