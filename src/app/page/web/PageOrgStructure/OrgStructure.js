import React, { Component } from 'react'

import styled               from 'styled-components'

import Body                 from 'app/page/web/PageOrgStructure/SectionContent'


const Div = styled.div`
  width: ${ props => props.shift ? 'calc(100% - 320px)' : '100%' };
  margin-left: ${ props => props.shift ? '320px' : '0px' };
  height: 100%;
`


class OrgStructure extends Component {
  constructor() {
    super()
  }

  render() {
    return (
      <Body />
    )
  }
}

export default OrgStructure

