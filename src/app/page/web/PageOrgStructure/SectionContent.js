import React, { Component } from 'react'

import styled               from 'styled-components'


const Container = styled.div`
  width: 100%;
  height: calc(100% - 50px);
`

class SectionContent extends Component {
  constructor() {
    super()
  }

  render() {
    return (
      <Container>
        OrgStructure
      </Container>
    )
  }
}

export default SectionContent