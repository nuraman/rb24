import { combineReducers }  from 'redux'


import period               from 'app/page/web/header/reducer'
import login                from 'app/page/web/PageLogin/reducer'
import kpi                  from 'app/page/web/header/employee/reducer'


export default combineReducers({
    period: period.reducer,
    login: login.reducer,
    kpi: kpi.reducer
  })
