import history              from './history'

export default  (location) => {
    history.push(location)
}
