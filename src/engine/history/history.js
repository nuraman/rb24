import createBrowserHistory from 'history/createBrowserHistory'

export default process.env.REACT_APP_BROWSER && createBrowserHistory()
