export default {
    Home: {
        pathname: '/',
        title: 'Home'
    },
    Customer: {
        pathname: '/Customer',
        title: 'Customer'
    },
    AudienceCreate: {
        pathname: '/AudienceCreate',
        title: 'AudienceCreate'
    },
    AudienceEdit: {
        pathname: '/AudienceEdit',
        title: 'AudienceEdit'
    }
}
