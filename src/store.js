import { createStore }      from 'redux'

import createReducer        from './createReducer'

let store

export default (payload) => {
  if (store) return store

  store = createStore(
    createReducer
  )

  return store
}
